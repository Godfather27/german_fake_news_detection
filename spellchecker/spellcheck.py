import csv
from spellchecker import SpellChecker
import re

if __name__ == "__main__":
	spell = SpellChecker(language="de")
	# the dictonary was extended by the author
	# dialect words, town names, names, business names
	spell.word_frequency.load_text_file("dict.txt")

	def normalize_word(word):
		word = word.lower()
		word = word.strip()
		# ignore hashtags
		if re.match(r"^#\w*", word): return ""
		match = re.search(r"\w+", word)
		return match.group() if match else ""

	def check_words(words):
		wrong_words=[]
		for word in words:
			stripped_word = normalize_word(word)
			if stripped_word not in spell:
				wrong_words.append(stripped_word)
		return wrong_words

	def write_unknown_words(words):
		unknown_words_file = open("unknown.txt", "w+")
		for word, amount in sorted(words.items(), reverse=True, key=lambda x: x[1]):
			unknown_words_file.write(str(amount) + " " + word + "\n")
		unknown_words_file.close()
		print("file written")

	def update_dict(words, word_dict):
		for word in words:
			if word in word_dict:
				word_dict[word] += 1
			else:
				word_dict[word] = 1

	def find_all_wrong(csv_reader):
		unique_words = set()
		all_errors_weighted = {}
		line_count = 0
		for row in csv_reader:
			post_words = re.split(r"[\n \.,\|]|http[^ \n]*", row["content"])
			unique_words.update(post_words)
			update_dict(check_words(post_words), all_errors_weighted)
			line_count += 1

		print(f'Processed {line_count} lines.')
		print("all words:", len(unique_words))
		print("unknown words:", len(all_errors_weighted))
		write_unknown_words(all_errors_weighted)

	def count_wrong(csv_reader):
		for idx, row in enumerate(csv_reader):
			post_words = re.split(r"[\n \.,\|]|http[^ \n]*", row["content"])
			wrong_words = check_words(post_words)	
			wrong = 0
			for word in post_words:
				if word in wrong_words and word != "":
					wrong += 1
			print(idx, wrong)	
	
	def main():
		
		with open('filtered_set.csv', mode='r') as csv_file:
			csv_reader = csv.DictReader(csv_file, delimiter=";")
			count_wrong(csv_reader)	
			
	main()
