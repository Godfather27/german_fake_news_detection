import re
import csv
import numpy as np
from collections import Counter
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import KFold
from sklearn.svm import LinearSVC
import matplotlib.pyplot as plt

german_stopwords = stopwords.words('german')

## LOAD DATA
target = []
posts_train = []
posts_test = []

with open('../sorted_tweets.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    for row in reader:
        target.append(row["label"])
        posts_train.append(row["text"])

with open('../filtered_set.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        posts_test.append(row["content"])


## PREPROCESS POSTS

SPECIAL_CHARACTERS = re.compile(r"[@<>.;:!\'?,\"()\[\]]")
stemmer = SnowballStemmer("german")
def get_stemmed_text(post):
    return ' '.join([stemmer.stem(word) for word in post.split()])

def get_trimmed_post(post):
    return SPECIAL_CHARACTERS.sub("", post)

def get_lower_case(post):
    return post.lower()

posts_train_clean = list(map(get_stemmed_text, map(get_trimmed_post, map(get_lower_case, posts_train))))
posts_test_clean = list(map(get_stemmed_text, map(get_trimmed_post, map(get_lower_case, posts_test))))

## TRAIN MODEL

target = np.array(target)
ngram_vectorizer = TfidfVectorizer(ngram_range=(1,3), binary=True, stop_words=german_stopwords) # performed better than CountVectorizer
# ngram_vectorizer = CountVectorizer(binary=True, ngram_range=(1, 3), stop_words=german_stopwords)
ngram_vectorizer.fit(posts_train_clean)
X = ngram_vectorizer.transform(posts_train_clean)
X_test = ngram_vectorizer.transform(posts_test_clean)

def find_best_C():
    k_fold = KFold(n_splits=5, random_state=0)
    metrics = []
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X)):
        print("fold %s" % fold)
        print("C\tAccurary\t\tPrecision\t\tRecall\t\t\tF1-Score")
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for C in np.arange(0.1,1.1,.1):
            svm = LinearSVC(C=round(C, 1))
            svm.fit(X_train, y_train)
            y_pred = svm.predict(X_val)
            c = round(C, 1)
            a = accuracy_score(y_val, y_pred)
            p = precision_score(y_val, y_pred, average="micro")
            r = recall_score(y_val, y_pred, average="micro")
            f = f1_score(y_val, y_pred, average="micro")
            metrics.append([a,p,r,f])
            print("%s\t%s\t%s\t%s\t%s" % (c,a,p,r,f))
    metrics = np.array(metrics)
    print("\t\tC\tAccurary\t\tPrecision\t\tRecall\t\t\tF1-Score")
    for i in range(10):
        print("average: %s\t%s\t%s\t%s\t%s" % ((i+1)/10, np.average(metrics[:,0][i::10]),np.average(metrics[:,1][i::10]),np.average(metrics[:,2][i::10]),np.average(metrics[:,3][i::10])))

def label_to_nu(label):
    return {
        "neutral": 0,
        "positive": 1,
        "negative": -1
    }[label]

# PREDICT GERMAN DATASET POSTS
def predict():
    svm = LinearSVC(C=0.1) # C is best fit according to cross-validation
    svm.fit(X, target)
    for label in svm.predict(X_test):
        print(label_to_nu(label))

# find_best_C()
predict()