import sys
sys.path.append("../utils")
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils.testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.preprocessing import StandardScaler
from get_dataset import get_preprocessed_dataset
from pipeline import pipeline_factory

def print_model(model):
    print()
    print("#"*20)
    print(model.__class__.__name__)

@ignore_warnings(category=UserWarning)
@ignore_warnings(category=ConvergenceWarning)
def show_best_features(X, target, model):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = []
    all_pred = []
    scores = []
    for train_indices, test_indices in k_fold.split(X, target):
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        model.fit(X_train, y_train)
        y_pred = model.predict(X_val)
        all_val += y_val.tolist()
        all_pred += y_pred.tolist()
    if hasattr(model, "feature_importances_"):
        feat_importances = pd.Series(model.feature_importances_, index=labels)
    elif hasattr(model, "coef_"):
        feat_importances = pd.Series(model.coef_[0], index=labels)
    else:
        for feature in range(len(labels)):
            score = cross_val_score(model, X[:,feature].reshape(-1, 1), target, cv=10)
            scores.append(score.mean())
        feat_importances = pd.Series(scores, index=labels)
    feat_importances.plot(kind='barh')
    plt.show()

@ignore_warnings(category=UserWarning)
@ignore_warnings(category=ConvergenceWarning)
def show_performance(x_type, X, target, model, print_all=False, print_m=False):
    if print_m: print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = []
    all_pred = []
    scores = []
    for train_indices, test_indices in k_fold.split(X, target):
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        model.fit(X_train, y_train)
        y_pred = model.predict(X_val)

        all_val += y_val.tolist() # append y_val to list of C
        all_pred += y_pred.tolist() # append y_pred to list of C
        if hasattr(model, "decision_function"):
            scores += model.decision_function(X_val).tolist()
        elif hasattr(model, "predict_proba"):
            scores += model.predict_proba(X_val)[:, 1].tolist()
        
    if print_all:
        if print_m:
            print("average for {} folds".format(splits))
            print("acc\tprec\trecall\tf1-sc\tROC_AUC")
        report = classification_report(all_val, all_pred, output_dict=True)
        print(
            "{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t\t{}".format(
                report["accuracy"],
                report["macro avg"]["precision"],
                report["macro avg"]["recall"],
                report["macro avg"]["f1-score"],
                roc_auc_score(all_val, scores),
                x_type
            )
        )
    else:
        return classification_report(all_val, all_pred)

def find_best_C(X, target, model,
    print_all=False,
    plot=False,
    range_start = .1,
    range_end = 50,
    step_size = .1
):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for _i, C in enumerate(np.arange(range_start, range_end, step_size)):
            model.set_params(C=C)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (C not in all_val):
                all_val[C] = []
                all_pred[C] = []
                scores[C] = []
            all_val[C] += y_val.tolist() # append y_val to list of C
            all_pred[C] += y_pred.tolist() # append y_pred to list of C
            scores[C] += model.decision_function(X_val).tolist()
    if print_all:
        print("average for all folds")
        print("C\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    c_values = []
    best = {}
    for C, y_true in all_val.items():
        y_pred = all_pred[C]
        score = scores[C]
        report = classification_report(y_true, y_pred, output_dict=True)
        c_values.append(C)
        accuracy.append(report["accuracy"])
        best[C] = report["accuracy"]
        if print_all:
            print(
                "{:.1f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    C,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, c_values)
        plt.xlabel("accuracy")
        plt.ylabel("c_values")
        plt.show()
    print("best C:", best_pair[0])
    return best_pair[0]

def find_best_smoothness(X, target, model,
    print_all=False,
    plot=False,
    decimals = 20
):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for exponent in range(decimals):
            smoothness = 1/10 ** exponent
            model.set_params(var_smoothing=smoothness)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (smoothness not in all_val):
                all_val[smoothness] = []
                all_pred[smoothness] = []
                scores[smoothness] = []
            all_val[smoothness] += y_val.tolist() # append y_val to list of smoothness
            all_pred[smoothness] += y_pred.tolist() # append y_pred to list of smoothness
            scores[smoothness] += model.predict_proba(X_val)[:, 1].tolist()
    if print_all:
        print("average for all folds")
        print("smoothness\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    smoothness_values = []
    best = {}
    for smoothness, y_true in all_val.items():
        y_pred = all_pred[smoothness]
        score = scores[smoothness]
        report = classification_report(y_true, y_pred, output_dict=True)
        smoothness_values.append(smoothness)
        accuracy.append(report["accuracy"])
        best[smoothness] = report["accuracy"]
        if print_all:
            print(
                "{}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    smoothness,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, smoothness_values)
        plt.xlabel("accuracy")
        plt.ylabel("smoothness_values")
        plt.show()
    print("best smoothness:", best_pair[0])
    return best_pair[0]

def find_best_tol(X, target, model,
    print_all=False,
    plot=False,
    decimals = 20
):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for exponent in range(decimals):
            tol = 1/10 ** exponent
            model.set_params(tol=tol)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (tol not in all_val):
                all_val[tol] = []
                all_pred[tol] = []
                scores[tol] = []
            all_val[tol] += y_val.tolist() # append y_val to list of tol
            all_pred[tol] += y_pred.tolist() # append y_pred to list of tol
            scores[tol] += model.predict_proba(X_val)[:, 1].tolist()
    if print_all:
        print("average for all folds")
        print("tol\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    tol_values = []
    best = {}
    for tol, y_true in all_val.items():
        y_pred = all_pred[tol]
        score = scores[tol]
        report = classification_report(y_true, y_pred, output_dict=True)
        tol_values.append(tol)
        accuracy.append(report["accuracy"])
        best[tol] = report["accuracy"]
        if print_all:
            print(
                "{}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    tol,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, tol_values)
        plt.xlabel("accuracy")
        plt.ylabel("tol_values")
        plt.show()
    print("best tol:", best_pair[0])
    return best_pair[0]

@ignore_warnings(category=UserWarning)
def find_best_alpha(X, target, model,
    print_all=False,
    plot=False,
    range_start=0, range_end=5, step_size=1,
):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for alpha in range(range_start, range_end, step_size):
            model.set_params(alpha=alpha)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (alpha not in all_val):
                all_val[alpha] = []
                all_pred[alpha] = []
                scores[alpha] = []
            all_val[alpha] += y_val.tolist() # append y_val to list of alpha
            all_pred[alpha] += y_pred.tolist() # append y_pred to list of alpha
            scores[alpha] += model.predict_proba(X_val)[:, 1].tolist()
    if print_all:
        print("average for all folds")
        print("alpha\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    alpha_values = []
    best = {}
    for alpha, y_true in all_val.items():
        y_pred = all_pred[alpha]
        score = scores[alpha]
        report = classification_report(y_true, y_pred, output_dict=True)
        alpha_values.append(alpha)
        accuracy.append(report["accuracy"])
        best[alpha] = report["accuracy"]
        if print_all:
            print(
                "{}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    alpha,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, alpha_values)
        plt.xlabel("accuracy")
        plt.ylabel("alpha_values")
        plt.show()
    print("best alpha:", best_pair[0])
    return best_pair[0]

def find_best_p(X, target, model, range_start=1, range_end=21, step_size=1, print_all=False, plot=False):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for p in range(range_start, range_end, step_size):
            model.set_params(p=p)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (p not in all_val):
                all_val[p] = []
                all_pred[p] = []
                scores[p] = []
            all_val[p] += y_val.tolist() # append y_val to list of p
            all_pred[p] += y_pred.tolist() # append y_pred to list of p
            scores[p] += model.predict_proba(X_val)[:, 1].tolist()
    if print_all:
        print("average for all folds")
        print("p\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    p_values = []
    best = {}
    for p, y_true in all_val.items():
        y_pred = all_pred[p]
        score = scores[p]
        report = classification_report(y_true, y_pred, output_dict=True)
        p_values.append(p)
        accuracy.append(report["accuracy"])
        best[p] = report["accuracy"]
        if print_all:
            print(
                "{}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    p,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, p_values)
        plt.xlabel("accuracy")
        plt.ylabel("p_values")
        plt.show()
    print("best p:", best_pair[0])
    return best_pair[0]

def find_best_neighbor_n(X, target, model, range_start=1, range_end=21, step_size=1, print_all=False, plot=False):
    print_model(model)
    splits = 10
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for n_neighbors in range(range_start, range_end, step_size):
            model.set_params(n_neighbors=n_neighbors)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_val)

            if (n_neighbors not in all_val):
                all_val[n_neighbors] = []
                all_pred[n_neighbors] = []
                scores[n_neighbors] = []
            all_val[n_neighbors] += y_val.tolist() # append y_val to list of n_neighbors
            all_pred[n_neighbors] += y_pred.tolist() # append y_pred to list of n_neighbors
            scores[n_neighbors] += model.predict_proba(X_val)[:, 1].tolist()
    if print_all:
        print("average for all folds")
        print("p\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    accuracy = []
    n_neighbors_values = []
    best = {}
    for n_neighbors, y_true in all_val.items():
        y_pred = all_pred[n_neighbors]
        score = scores[n_neighbors]
        report = classification_report(y_true, y_pred, output_dict=True)
        n_neighbors_values.append(n_neighbors)
        accuracy.append(report["accuracy"])
        best[n_neighbors] = report["accuracy"]
        if print_all:
            print(
                "{}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
                    n_neighbors,
                    report["accuracy"],
                    report["macro avg"]["precision"],
                    report["macro avg"]["recall"],
                    report["macro avg"]["f1-score"],
                    roc_auc_score(y_true, score)
                )
            )
    best_pair = max(best.items(), key=lambda element: element[1])
    if plot:
        plt.plot(accuracy, n_neighbors_values)
        plt.xlabel("accuracy")
        plt.ylabel("n_neighbors_values")
        plt.show()
    print("best neighbor_n:", best_pair[0])
    return best_pair[0]

def find_best_features(X, y):
    # vgl. https://towardsdatascience.com/feature-selection-techniques-in-machine-learning-with-python-f24e7da3f36e
    bestfeatures = SelectKBest(score_func=f_classif, k=4)
    fit = bestfeatures.fit(X,y)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(labels)
    featureScores = pd.concat([dfcolumns,dfscores],axis=1)
    featureScores.columns = ['Specs','Score']
    print(featureScores.nlargest(10,'Score'))

dataset, vectorized_content = get_preprocessed_dataset()
pure_dataset = pd.read_csv('../filtered_set.csv')
labels = ["word_count", "attachment", "post", "punctuation", "emoji", "spelling", "grammar", "misc", "sentiment"]
mixed_content = pure_dataset[["word_count", "attachment", "post", "punctuation", "emoji", "spelling", "grammar", "misc", "sentiment", "content"]].values
numerical_content = dataset[labels].values
y = dataset["label"].values
best_features = dataset[["word_count", "attachment", "post", "spelling"]].values


### FIND OPTIMAL PARAMETERS

find_best_features(numerical_content, y)

# find_best_C(numerical_content, y, LogisticRegression(random_state=0, solver='liblinear')) # 26.0
# find_best_tol(numerical_content, y, LogisticRegression(C=26.0, random_state=0, solver='liblinear'), print_all=True, plot=True) # 0.0001
# find_best_C(dataset[["spelling", "grammar", "misc", "punctuation"]].values, y, LogisticRegression(random_state=0, solver='liblinear'), print_all=True, plot=True) # 0.7
# find_best_tol(dataset[["spelling", "grammar", "misc", "punctuation"]].values, y, LogisticRegression(C=0.7, random_state=0, solver='liblinear'), print_all=True, plot=True) # 0.1
# find_best_C(best_features, y, LogisticRegression(random_state=0, solver='liblinear'), print_all=True, plot=True) # 0.9
# find_best_tol(best_features, y, LogisticRegression(C=0.9, random_state=0, solver='liblinear'), print_all=True, plot=True) # 0.001
# find_best_C(numerical_content, y, LinearSVC(random_state=0)) # 2.0
# find_best_smoothness(numerical_content, y, GaussianNB(), print_all=True, plot=True) # 0.001
# find_best_p(numerical_content, y, KNeighborsClassifier(), print_all=True, plot=True) # 9
# find_best_C(vectorized_content, y, LogisticRegression(random_state=0, solver='liblinear'), print_all=True, plot=True) # 26.1
# find_best_tol(vectorized_content, y, LogisticRegression(random_state=0, solver='liblinear', penalty='l2', dual=False, C=26.1), print_all=True, plot=True) # 1.0
# find_best_C(vectorized_content, y, LinearSVC(random_state=0)) # 1.2
# find_best_alpha(vectorized_content, y, MultinomialNB(), print_all=True, plot=True) # 0
# find_best_neighbor_n(vectorized_content, y, KNeighborsClassifier(), print_all=True, plot=True) # 2


### GET FEATURES THAT PERFORM THE BEST FOR EACH MODEL

# show_best_features(numerical_content, y, LogisticRegression(C=26.0, tol=0.0001, penalty="l2", dual=False, fit_intercept=False, multi_class="ovr", random_state=0, solver='liblinear')) # can show
# show_best_features(numerical_content, y, LinearSVC(C=2.0, random_state=0)) # can show
# show_best_features(numerical_content, y, GaussianNB(var_smoothing=0.001)) # can't show
# show_best_features(numerical_content, y, DecisionTreeClassifier(random_state=0)) # can show
# show_best_features(numerical_content, y, KNeighborsClassifier(p=9)) # can't show

### PERFORMANCE COMPARISION

## LOG REG

# show_performance("meta-data", numerical_content, y, LogisticRegression(C=26.0, tol=0.0001, penalty="l2", dual=False, fit_intercept=False, multi_class="ovr", random_state=0, solver='liblinear'), print_all=True, print_m=True) # best C is 26.0
# show_performance("best meta-data", dataset[["spelling", "grammar", "misc", "punctuation"]].values, y, LogisticRegression(C=0.7, tol=0.1, solver='liblinear', fit_intercept=True, multi_class="ovr", random_state=0), print_all=True)
# show_performance("bestk meta-data", best_features, y, LogisticRegression(C=0.9, tol=0.001, solver='liblinear', fit_intercept=True, multi_class="ovr", random_state=0), print_all=True)
# show_performance("tf-idf", vectorized_content, y, LogisticRegression(C=26.1, random_state=0, penalty="l2", dual=False, solver='liblinear', tol=1.0, fit_intercept=False, multi_class="ovr"), print_all=True)
# show_performance("tf-idf + meta-data", mixed_content, y, pipeline_factory(LogisticRegression(C=26.1, random_state=0, solver='liblinear')), print_all=True)

## LIN SVM

# show_performance("meta-data", numerical_content, y, LinearSVC(C=2.0, random_state=0), print_all=True, print_m=True) # best C is 2.0
# show_performance("best meta-data", dataset[["spelling", "grammar", "misc", "punctuation", "post"]].values, y, LinearSVC(C=2.0, random_state=0), print_all=True) # better
# show_performance("tf-idf", vectorized_content, y, LinearSVC(C=1.2, random_state=0), print_all=True)
# show_performance("tf-idf + meta-data", mixed_content, y, pipeline_factory(LinearSVC(C=1.2, random_state=0)), print_all=True)

## Naive Bayes

# show_performance("meta-data", numerical_content, y, GaussianNB(var_smoothing=0.001), print_all=True, print_m=True) # best var_smoothing is 0.001
# show_performance("best meta-data", dataset[["spelling", "post", "attachment", "word_count"]].values, y, GaussianNB(var_smoothing=0.001), print_all=True) # equal
# show_performance("tf-idf", vectorized_content, y, MultinomialNB(alpha=0), print_all=True)
# show_performance("tf-idf + meta-data", mixed_content, y, pipeline_factory(GaussianNB(var_smoothing=0.001)), print_all=True)

## Decision Tree

# show_performance("meta-data", numerical_content, y, DecisionTreeClassifier(random_state=0), print_all=True, print_m=True) # splitter best, better than random # gini better than entropy
# show_performance("best meta-data", dataset[["misc", "grammar", "spelling", "attachment", "word_count"]].values, y, DecisionTreeClassifier(random_state=0), print_all=True) # better
# show_performance("tf-idf", vectorized_content, y, DecisionTreeClassifier(random_state=0), print_all=True)
# show_performance("tf-idf + meta-data", mixed_content, y, pipeline_factory(DecisionTreeClassifier(random_state=0)), print_all=True)

## K Neighbor

# show_performance("meta-data", numerical_content, y, KNeighborsClassifier(p=9), print_all=True, print_m=True) # better than weights="distance" # algorithm performance is equal # all leafe sizes equally good # best p is 9
# show_performance("best meta-data", dataset[["word_count", "attachment", "post", "punctuation", "spelling", "grammar", "sentiment"]].values, y, KNeighborsClassifier(p=9), print_all=True) # equal
# show_performance("tf-idf", vectorized_content, y, KNeighborsClassifier(n_neighbors=2), print_all=True)
# show_performance("tf-idf + meta-data", mixed_content, y, pipeline_factory(KNeighborsClassifier(n_neighbors=2)), print_all=True)
