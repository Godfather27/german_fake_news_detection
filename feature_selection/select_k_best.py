import re
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import mutual_info_classif
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
import sys
sys.path.append("../utils")
from get_dataset import get_preprocessed_dataset

dataset, _ = get_preprocessed_dataset()

X = dataset[["attachment", "post", "punctuation", "emoji", "spelling", "grammar", "misc", "sentiment"]].values
y = dataset["label"].values

selector = SelectKBest(score_func=mutual_info_classif, k="all")
fit = selector.fit(X, y)
print(fit.scores_)