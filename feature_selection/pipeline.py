import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import TruncatedSVD
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
import pandas as pd
import numpy as np

stemmer = SnowballStemmer("german")
SPECIAL_CHARACTERS = re.compile(r"[@<>.;:!„#…/\-\+\'?,\"()\[\]]")
german_stopwords = stopwords.words('german')

dataset = pd.read_csv('../filtered_set.csv')

class MyNormalizer:
  def __init__(self):
    pass
  
  def fit(self, X, y=None):
    return self
  
  def normalize(self, row):
    return [row[0], row[1]/row[0], row[2]/row[0], row[3]/row[0], row[4]/row[0], row[5]/row[0], row[6]/row[0], row[7]/row[0], row[8]/row[0]]

  def transform(self, X):    
    return list(map(self.normalize, X))

class MyTextNormalizer:
  def __init__(self):
    pass
  
  def fit(self, X, y=None):
    return self
  
  def transform(self, X):
    posts = []
    for row in X:
      for post in row:
        posts.append(self.process_posts(post))
    return posts
  
  def get_stemmed_text(self, word):
    return stemmer.stem(word)

  def get_trimmed_post(self, word):
      return SPECIAL_CHARACTERS.sub(" ", word)

  def get_lower_case(self, word):
      return word.lower()

  def process_posts(self, post):
      return re.sub(' +', ' ', " ".join(map(self.get_stemmed_text, map(self.get_trimmed_post, map(self.get_lower_case, post.split())))))

class MySelector:
  def __init__(self, type):
    self.type = type
  
  def fit(self, X, y=None):
    return self
  
  def transform(self, X):
    of_type = []
    for row in X:
      row_features = []
      for feature in row:
        if type(feature) == int and type(feature) == self.type: 
          row_features.append(feature)
        if type(feature) == str and type(feature) == self.type:
          of_type.append(feature)
      if self.type == int:
        of_type.append(row_features)
    if self.type == int:
      return of_type
    return np.array(of_type).reshape(-1,1)

def pipeline_factory(model):
    numerical_pipe = Pipeline([
      ('select_numerical', MySelector(type=int)),
      ('normalize', MyNormalizer())
    ])

    text_pipe = Pipeline([
      ('select_text', MySelector(type=str)),
      ('normalize', MyTextNormalizer()),
      ('tfidf', TfidfVectorizer(ngram_range=(1,3), binary=True, stop_words=german_stopwords))
    ])

    full_pipeline = Pipeline([
      ('feat_union', FeatureUnion([
        ('text_pipeline', text_pipe),
        ('numerical_pipeline', numerical_pipe)
      ])),
      ('reduce_dim', TruncatedSVD()),
      ('classify', model)
    ])

    return full_pipeline
