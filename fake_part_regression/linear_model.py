import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import mean_squared_error, r2_score
import csv
import re
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt

WORD_COUNT = 0
TRUE_PART_WORD_COUNT = 1
FAKE_PART_WORD_COUNT = 2
NEUTRAL_PART_WORD_COUNT = 3
ATTACHMENT_WORD_COUNT = 4
POST_WORD_COUNT = 5
PUNCTUATION_MARKS_COUNT = 6
EMOJI_COUNT = 7
SPELLING_ERROS = 8
GRAMMAR_ERROS = 9
MISCH_WRITING_ERRORS = 10
SENTIMENT = 11

german_stopwords = stopwords.words('german')
SPECIAL_CHARACTERS = re.compile(r"[@<>.;:!\'?,\"()\[\]]")
stemmer = SnowballStemmer("german")

def load_dataset():
  target = []
  posts = []
  features = []
  with open('../filtered_set.csv') as csvfile:
      reader = csv.reader(csvfile)
      for row in reader:
          target.append(row[1])
          posts.append(row[2])
          features.append(row[-12:])
  return [target, posts, np.asarray(features, dtype=np.int)]

def get_stemmed_text(post):
    return ' '.join([stemmer.stem(word) for word in post.split()])

def get_trimmed_post(post):
    return SPECIAL_CHARACTERS.sub("", post)

def get_lower_case(post):
    return post.lower()

def process_posts(posts):
    return list(map(get_stemmed_text, map(get_trimmed_post, map(get_lower_case, posts))))

# X independent variable
# Y dependent variable


target, posts, features = load_dataset()
# posts_processed = process_posts(posts)

X = features[:,FAKE_PART_WORD_COUNT]
y = features[:,WORD_COUNT]
# y = np.delete(features, [FAKE_PART_WORD_COUNT],1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5)

print(X_train)
print()
print(X_train.reshape(-1,1))

lr = LinearRegression()
lr.fit(X_train, y_train)
predictions = lr.predict(X_test)
# predictions = cross_val_predict(lr, X, y, cv=10)

print("Mean squared error: %.2f" % mean_squared_error(y_test, predictions), 'Variance score: %.2f' % r2_score(y_test, predictions))

plt.scatter(X_test, y_train,  color='black')
plt.plot(X_test, predictions, color='blue', linewidth=3)

plt.xticks(())
plt.yticks(())
plt.ylabel("FAKE_PART_WORD_COUNT")
plt.xlabel("WORD_COUNT")

plt.show()