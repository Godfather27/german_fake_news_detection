import random
import pandas as pd
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_predict, cross_val_score
from sklearn.metrics import r2_score
import re
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt

plt.tight_layout()

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

WORD_COUNT = "word_count"
TRUE_PART = "true_part"
FAKE_PART = "fake_part"
NEUTRAL_PART = "neutral_part"
ATTACHMENT = "attachment"
POST = "post"
PUNCTUATION_MARKS_COUNT = "punctuation"
EMOJI_COUNT = "emoji"
SPELLING_ERRORS = "spelling"
GRAMMAR_ERRORS = "grammar"
MISC_WRITING_ERRORS = "misc"
SENTIMENT = "sentiment"

german_stopwords = stopwords.words('german')
SPECIAL_CHARACTERS = re.compile(r"[@<>.;:!\'?,\"()\[\]]")
stemmer = SnowballStemmer("german")

def get_stemmed_text(post):
    return ' '.join([stemmer.stem(word) for word in post.split()])

def get_trimmed_post(post):
    return SPECIAL_CHARACTERS.sub("", post)

def get_lower_case(post):
    return post.lower()

def process_posts(posts):
    return list(map(get_stemmed_text, map(get_trimmed_post, map(get_lower_case, posts))))

# X independent variable
# Y dependent variable

dataset = pd.read_csv('../filtered_set.csv')
dataset_fake = dataset.loc[dataset['label'] == 'fake']
dataset_true = dataset.loc[dataset['label'] == 'truth']
absolute_numerics = dataset.select_dtypes(include="int64")
absolute_numerics_fake = dataset_fake.select_dtypes(include="int64")
absolute_numerics_true = dataset_true.select_dtypes(include="int64")

print(dataset)
exit()

# DATAPOINT RELATIVE TO WORD_COUNT
relative_numerics = absolute_numerics.copy()
normalized = relative_numerics[[TRUE_PART, FAKE_PART,
    NEUTRAL_PART, ATTACHMENT, POST,
    PUNCTUATION_MARKS_COUNT, EMOJI_COUNT, SPELLING_ERRORS,
    GRAMMAR_ERRORS, MISC_WRITING_ERRORS]].div(relative_numerics[WORD_COUNT], axis=0)
relative_numerics.update(normalized)

relative_numerics_fake = absolute_numerics_fake.copy()
normalized = relative_numerics_fake[[TRUE_PART, FAKE_PART,
    NEUTRAL_PART, ATTACHMENT, POST,
    PUNCTUATION_MARKS_COUNT, EMOJI_COUNT, SPELLING_ERRORS,
    GRAMMAR_ERRORS, MISC_WRITING_ERRORS]].div(relative_numerics_fake[WORD_COUNT], axis=0)
relative_numerics_fake.update(normalized)

relative_numerics_true = absolute_numerics_true.copy()
normalized = relative_numerics_true[[TRUE_PART, FAKE_PART,
    NEUTRAL_PART, ATTACHMENT, POST,
    PUNCTUATION_MARKS_COUNT, EMOJI_COUNT, SPELLING_ERRORS,
    GRAMMAR_ERRORS, MISC_WRITING_ERRORS]].div(relative_numerics_true[WORD_COUNT], axis=0)
relative_numerics_true.update(normalized)

# # COMPARISON PLOTS
# print("\nabsolute\n", absolute_numerics.corr(method="spearman"))
# print("\nrelative\n", relative_numerics.corr())

# # PLOT PAIRPLOTS
# sns.set(style="ticks", color_codes=True)
# sns.pairplot(absolute_numerics)
# sns.pairplot(relative_numerics)

features = [WORD_COUNT, ATTACHMENT, POST, PUNCTUATION_MARKS_COUNT, EMOJI_COUNT, SPELLING_ERRORS, GRAMMAR_ERRORS, MISC_WRITING_ERRORS, SENTIMENT]

def linear_regression(lr_data):
  y_features = [FAKE_PART]
  for feature in y_features:
    X = lr_data[features]
    y = lr_data[feature]
    # X = relative_numerics[features]
    # y = relative_numerics[feature]

    cv = 10
    lr = LinearRegression()

    # evaulates the performance of the 
    y_pred = cross_val_predict(lr, X, y, cv=cv)
    scores = cross_val_score(lr, X, y, cv=cv)
    print(feature)
    print(scores)
    print("R2 Score: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

  plt.scatter(y, y_pred, edgecolors=(0, 0, 0))
  plt.plot([y.min(), y.max()], [y.min(), y.max()], lw=2)
  plt.xlabel('Measured')
  plt.ylabel('Predicted')
  plt.show()

def random_model(lr_data):
  y = lr_data[FAKE_PART]
  X = lr_data[features]
  random.seed(a=len(X))
  y_pred = [random.random() for x in range(len(X))]

  print("R2 Score: %0.2f" % (r2_score(y, y_pred)))

  plt.scatter(y, y_pred, edgecolors=(0, 0, 0))
  plt.plot([y.min(), y.max()], [y.min(), y.max()], lw=2)
  plt.xlabel('Measured')
  plt.ylabel('Predicted')
  plt.show()

random_model(relative_numerics)