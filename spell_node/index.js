let foo = require("./result.json");

function errorFactory() {
  return {
    "GRAMMAR": 0,
    "SPELLING": 0,
    "MISC": 0,
  }
}

function check(result) {
  errors = errorFactory();
  result.matches.forEach(match => {
    switch(match.rule.category.id) {
      case "COMPOUNDING":
      case "EMPFOHLENE_RECHTSCHREIBUNG":
      case "TYPOS":
        errors["SPELLING"]++;
        return;
      case "PUNCTUATION":
      case "CONFUSED_WORDS":
      case "GRAMMAR":
      case "HILFESTELLUNG_KOMMASETZUNG":
      case "CASING":
      case "SEMANTICS":
        errors["GRAMMAR"]++;
        return;
      case "COLLOQUIALISMS":
      case "TYPOGRAPHY":
      case "MISC":
      case "REDUNDANCY":
      case "STYLE":
      case "IDIOMS":
      default:
        errors["MISC"]++;
        return;
    }
  });
  console.log("SPELL\tGRAMM\tMISC")
  console.log(`${errors.SPELLING}\t${errors.GRAMMAR}\t${errors.MISC}`);
}

check(foo);