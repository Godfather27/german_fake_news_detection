import re
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import mutual_info_classif
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt

plt.tight_layout()
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

WORD_COUNT = "word_count"
TRUE_PART = "true_part"
FAKE_PART = "fake_part"
NEUTRAL_PART = "neutral_part"
ATTACHMENT = "attachment"
POST = "post"
PUNCTUATION_MARKS_COUNT = "punctuation"
EMOJI_COUNT = "emoji"
SPELLING_ERRORS = "spelling"
GRAMMAR_ERRORS = "grammar"
MISC_WRITING_ERRORS = "misc"
SENTIMENT = "sentiment"

german_stopwords = stopwords.words('german')
SPECIAL_CHARACTERS = re.compile(r"[@<>.;:!„#…/\-\+\'?,\"()\[\]]")
stemmer = SnowballStemmer("german")

def get_stemmed_text(word):
    return stemmer.stem(word)

def get_trimmed_post(word):
    return SPECIAL_CHARACTERS.sub(" ", word)

def get_lower_case(word):
    return word.lower()

def process_posts(post):
    return re.sub(' +', ' ', " ".join(map(get_stemmed_text, map(get_trimmed_post, map(get_lower_case, post.split())))))

def get_preprocessed_dataset():
    # X independent variable
    # Y dependent variable
    dataset = pd.read_csv('../filtered_set.csv')
    absolute_numerics = dataset.select_dtypes(include="int64")

    # DATAPOINT RELATIVE TO WORD_COUNT
    relative_numerics = absolute_numerics.copy()
    normalized = relative_numerics[[TRUE_PART, FAKE_PART,
        NEUTRAL_PART, ATTACHMENT, POST,
        PUNCTUATION_MARKS_COUNT, EMOJI_COUNT, SPELLING_ERRORS,
        GRAMMAR_ERRORS, MISC_WRITING_ERRORS]].div(relative_numerics[WORD_COUNT], axis=0)
    dataset.update(normalized)
    dataset["content"] = dataset["content"].apply(process_posts)

    tfidfvectorizer = TfidfVectorizer(ngram_range=(1,3), binary=True, stop_words=german_stopwords)
    vectorized_content = tfidfvectorizer.fit_transform(dataset["content"])
    return [dataset, vectorized_content]