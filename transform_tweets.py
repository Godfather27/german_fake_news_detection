import csv

non_deleted = []
with open('./labeled_tweets.tsv') as tsvfile:
    reader = csv.reader(tsvfile, delimiter='\t')

    for row in reader:
        if row[4] != "Not Available":
            non_deleted.append([row[1], row[4]])

non_deleted = sorted(non_deleted, key=lambda x: x[0])

with open('./sorted_tweets.csv', mode='w') as tweetfile:
    employee_writer = csv.writer(tweetfile, delimiter=';')
    for row in non_deleted:
        employee_writer.writerow(row)
print("done");