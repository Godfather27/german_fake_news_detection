import random
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

dataset = pd.read_csv('../filtered_set.csv')
random.seed(len(dataset))

def predict_label():
  return "truth" if random.random() < 0.5 else "fake"

def random_categorizer(X):
  predicted = []
  for _label in X['label']:
    predicted.append(predict_label())
  return predicted

def score(target):
  scores = []
  for _ in range(1000):
    predicted = np.array(random_categorizer(dataset))
    scores.append([
      accuracy_score(target, predicted),
      precision_score(target, predicted, pos_label="fake"),
      recall_score(target, predicted, pos_label="fake"),
      f1_score(target, predicted, pos_label="fake")
    ])
  print("\t\tAccurary\t\tPrecision\t\tRecall\t\t\tF1-Score")
  scores = np.array(scores)
  print("average: %s\t%s\t%s\t%s" % (np.average(scores[:,0]),np.average(scores[:,1]),np.average(scores[:,2]),np.average(scores[:,3])))


score(dataset["label"].to_numpy())