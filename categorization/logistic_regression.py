import random
import re
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.model_selection import StratifiedKFold
import sys
sys.path.append("../utils")
from get_dataset import get_preprocessed_dataset

dataset, vectorized_content = get_preprocessed_dataset()

def find_best_C_tf_idf(X, target):
    splits = 10
    range_end = 29
    k_fold = StratifiedKFold(n_splits=splits, random_state=0)
    all_val = {}
    all_pred = {}
    scores = {}
    for fold, (train_indices, test_indices) in enumerate(k_fold.split(X, target)):
        print("fold %s" % fold)
        X_train, X_val, y_train, y_val = X[train_indices], X[test_indices], target[train_indices], target[test_indices]
        for _i, C in enumerate(np.arange(27,range_end + 1,.1)):
            lr = LogisticRegression(random_state=0, C=C, solver='liblinear')
            lr.fit(X_train, y_train)
            y_pred = lr.predict(X_val)

            if (C not in all_val):
                all_val[C] = []
                all_pred[C] = []
                scores[C] = []
            all_val[C] += y_val.tolist() # append y_val to list of C
            all_pred[C] += y_pred.tolist() # append y_pred to list of C
            scores[C] += lr.decision_function(X_val).tolist()
    print("average for all folds")
    print("C\tacc\tprec\trecall\tf1-sc\tROC_AUC")
    precisions = []
    c_values = []
    for C, y_true in all_val.items():
        y_pred = all_pred[C]
        score = scores[C]
        report = classification_report(y_true, y_pred, output_dict=True)
        c_values.append(C)
        precisions.append(report["macro avg"]["precision"])
        print("{:.1f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}".format(
            C,
            report["accuracy"],
            report["macro avg"]["precision"],
            report["macro avg"]["recall"],
            report["macro avg"]["f1-score"],
            roc_auc_score(y_true, score)
          )
        )

    plt.plot(precisions, c_values)
    plt.show()

# find_best_C_tf_idf(vectorized_content, dataset["label"].to_numpy())
find_best_meta_data(dataset[[]])
